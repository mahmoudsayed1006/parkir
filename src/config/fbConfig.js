import * as firebase from "firebase/app";
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';

// Replace this with your own config details
var config = {
  apiKey: "AIzaSyAPHObfK0ZyFG713imXCKQyGOyfa__j5UE",
  authDomain: "parkirapp-34d11.firebaseapp.com",
  databaseURL: "https://parkirapp-34d11.firebaseio.com",
  projectId: "parkirapp-34d11",
  storageBucket: "parkirapp-34d11.appspot.com",
  messagingSenderId: "913578532894",
  appId: "1:913578532894:web:d81d16147e0fa975044e0f",
  measurementId: "G-4WS1B7H4WB"
  };
  firebase.initializeApp(config);
  firebase.firestore();
  firebase.database() 

export default firebase ;
