import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, NavLink } from 'react-router-dom'
import firebase from '../../config/fbConfig'
import './barriers.css'
import PaypalBtn from 'react-paypal-checkout';
import {Modal,message} from 'antd';
import moment from 'moment'
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
class Barrier extends Component {
 state={
  
  grages:[{"Fine":10,"HourPrice":4}],
   images:[],
   modal1Visible: false,
   modal2Visible: false,
   step:1,
   reservation : this.props.location.data?this.props.location.data:[],
   reservation2 : [],
   hours:"",
   mins:"", 
   images :[],
   fine:false,
   numberOfHours:0,
   fineHours:0,
   reciept:false,
   doRate:3,
   lan:1,
   grage:this.props.location.data2?this.props.location.data2:[]
   
 }
 constructor(props){
  super(props)
  this.getGarage()
  console.log(this.props.location.data2) 
  
}
setModal1Visible(modal1Visible) {
  this.setState({ modal1Visible ,step:1});
}
setModal2Visible(modal2Visible) {
  this.setState({ modal2Visible});
}
getGarage = () =>{
  let arr1 = []
  firebase.database().ref('ParkirApp/GARAGE/FCI_Garage').on('value', (snapshot) => {
    arr1 = [];
    console.log(snapshot.val())
      arr1.push(snapshot.val())
      this.setState({
        grage: arr1,
      })
  })
}
getData2 = () =>{
    
  firebase.database().ref('ParkirApp/GARAGE').on('value', (snapshot) => {
    let grages = [];
    console.log(snapshot.val())
    snapshot.forEach(data => {
      const dataVal = data.val()
      grages.push({
        Address1: dataVal.Address1,
        location: dataVal.Address2,
        Fine:dataVal.Fine,
        HourPrice:dataVal.HourPrice,
        Name:dataVal.Name,
      })
    })
    this.setState({grages:grages})

  })
    
}

  getData = () =>{
    
    let uid = this.props.auth.uid;
      let arr =[];
      firebase.database().ref(`ParkirApp/Book/FCI_Garage/1/${uid}`).on('value', (snapshot) => {
        if (snapshot.val()) {
          console.log(snapshot.val())
          arr =[];
          arr.push(snapshot.val())
          this.setState({
            reservation: arr
          })
        }else{
          firebase.database().ref(`ParkirApp/Book/FCI_Garage/2/${uid}`).on('value', (snapshot) => {
            if (snapshot.val()) {
              console.log(snapshot.val())
              arr =[];
              arr.push(snapshot.val())
              this.setState({
                reservation: arr
              })
            }else{
              firebase.database().ref(`ParkirApp/Book/FCI_Garage/3/${uid}`).on('value', (snapshot) => {
                if (snapshot.val()) {
                  console.log(snapshot.val())
                  arr =[];
                  arr.push(snapshot.val())
                  this.setState({
                    reservation: arr
                  })
                }
              })
            }
          })
        }
        console.log(arr)
        this.setState({
          reservation: arr,
          reciept:true
        })
      })

  }
  getReciept = () => {
    let arr = [];
    let uid = this.props.auth.uid;
    firebase.database().ref(`ParkirApp/users/FCI_Garage/${uid}`).on('value', (snapshot) => {
      arr =[];
      
      if(snapshot.val() != null){
      arr.push(snapshot.val())
      this.setState({
        reservation2: arr,
        lan:arr[0].lan
      })

      var start  = arr[0].startDate +" "+ arr[0].startTime;
      var end = arr[0].endDate +" "+ arr[0].endTime;
      console.log(start)
      console.log(end)
      var ms = moment(end,"DD/MM/YYYY HH:mm:ss").diff(moment(start,"DD/MM/YYYY HH:mm:ss"));
      var d = moment.duration(ms);
      let hours = d.days() * 24 + d.hours();
      let mins =  d.minutes();
      console.log("hours" + hours + "mins" + d.minutes() )
      let finalHours1= hours;
      if(mins > 0 ){
        finalHours1 + 1;
      }
      /////////////
      var realEnd = new Date();
      var ms2 = moment(realEnd,"DD/MM/YYYY HH:mm:ss").diff(moment(start,"DD/MM/YYYY HH:mm:ss"));
      var d2 = moment.duration(ms2);
      let realHours = d2.days() * 24 + d2.hours();
      let realMins =  d2.minutes();
      console.log("hours" + realHours + "mins" + realMins )
      let finalHours2 = realHours;
      if(realMins > 0 ){
        finalHours2 + 1;
      }

      if(realHours > 0 && realHours > hours){
        this.setState({
          hours: realHours,
          mins:realMins,
          fine:true,
          numberOfHours:realHours,
          fineHours:finalHours2 - hours
        })
      } else{
        this.setState({
          hours: hours,
          mins:mins,
          fine:false,
          numberOfHours:hours,
          fineHours:finalHours1 - hours
        })
      }
      

    }
      //console.log(hours + ':' + mins);
     
    })
  }
  openGate = () =>{
    let l = message.loading('Wait', 2.5)
    let lan = this.state.reservation2[0].lan
    console.log(lan)
    firebase.database().ref(`ParkirApp/iot/FCI_Garage/${lan}`).update({
      state: 1,
      id:this.props.auth.uid,
      //time:""
    })
    .then(() => {
      l.then(() => message.success('gate will be open', 2.5))
    }).catch(err => {
      //console.log(err.response)
      l.then(() => message.error('err', 2.5))
    });
  }
  rate = () =>{
    let l = message.loading('Wait', 2.5)
    console.log(this.state.grage)
    let userCount = parseInt(this.state.grage[0].total_user) + 1;
    let rateCount = parseInt(this.state.grage[0].total_rate)+parseInt(this.state.doRate);
    console.log(userCount)
    console.log(rateCount)
    
    firebase.database().ref(`ParkirApp/GARAGE/FCI_Garage`).update({
      total_user:userCount,
      total_rate:rateCount,
      rate:rateCount / userCount
    }) .then(() => {
      l.then(() => message.success('rate sucess', 2.5))
    }).catch(err => {
      console.log(err.response)
      //l.then(() => message.error('err', 2.5))
    });
  }
  pay = () =>{
    this.setState({step:2})
    let l = message.loading('Wait', 2.5);
    let lan = this.state.reservation2[0].lan
    console.log(lan)
    
    firebase.database().ref(`ParkirApp/users/FCI_Garage/${this.props.auth.uid}`).update({
      checkPAY:true
    })
    .then(() => {
      firebase.database().ref(`ParkirApp/iot/FCI_Garage/${lan}`).set({
        state: 0,
        id:"",
        time:new Date().toISOString()
      })
      .then(() => {
        firebase.database().ref(`ParkirApp/Book/FCI_Garage/${lan}`).child(this.props.auth.uid).remove()
        .then(() => {
          
          l.then(() => message.success('leave sucess', 2.5))
          this.rate()
          //this.props.history.push('/')
        }).catch(err => {
          //console.log(err.response)
          l.then(() => message.error('err', 2.5))
        });
      }).catch(err => {
        //console.log(err.response)
        l.then(() => message.error('err', 2.5))
      });
      
    }).catch(err => {
      //console.log(err.response)
      l.then(() => message.error('err', 2.5))
    });
  }
  componentDidMount(){
    this.getReciept()
    this.getData()
  this.getData2()
  
  }
  
  render() {

    const client = {
      production:'EGDsqrKDqwHC78vcFhzs0iUOdf4HmfU7LEmcTKWWgqPBfs5S5rqMOrUmg04S6H97gO5VLBIsT4N5UdB8',
      sandbox: 'ATMMAuiuEpavorDUe_PFHh2s_qaWpbYPjruIt_tHTeycT1dEHEiP1LdDhTHJFvLAiVDATNxZNcXXpJyx',
  }
    const {auth } = this.props;
    const { reservation,reservation2,grages,doRate} = this.state;
        console.log(reservation2)
        console.log(this.state.grage)
   
    if (!auth.uid) return <Redirect to='/signin' /> 
    return (
      <div className="Barrier">
        <div className="titleDiv">
          <p>We Are In Your Services Any Time</p>
          {/*<img width="400" src="https://pngriver.com/wp-content/uploads/2018/04/Download-Toyota-Car-PNG-Picture.png"></img>
          */}{this.state.reservation.length > 0 &&
          <div>
          {/*<button 
            onClick={ () => this.setModal2Visible(true)}
            style={{background: 'orange',width: '150px',fontSize: '18px',marginBottom: '30px',outline:'none',cursor:"pointer",color: "#fff",border:"none",height: "30px",borderRadius: "25px"}} 
          >   Rate
          </button>*/}
          <p style={{fontWeight:300}}>Garage Name : <span style={{fontWeight:500}}>{grages[0].Name}</span></p>
          <p style={{fontWeight:300}}>Parking No : </p>
          <span>{this.state.lan}</span>
          <div className="myBook"> 
            <p style={{color:'#464646'}}>
              Begin at 
              <span class="material-icons" style={{color:'green',fontSize: '20px',margin: '2px 10px',position: 'absolute'}}>
              last_page
              </span>
            </p>
            <p>{reservation[0].startTime}<span style={{marginLeft: '20px'}}>{reservation[0].startDate}</span></p>
            <p style={{color:'#464646'}}>
              Departure at 
              <span class="material-icons" style={{color:'red',fontSize: '20px',margin: '2px 10px',position: 'absolute'}}>
              first_page
              </span>
            </p>
            <p>{reservation[0].endTime}<span style={{marginLeft: '20px'}}>{reservation[0].endDate}</span></p>
          </div>
          <div className="buttonDiv">
            <div onClick={this.openGate} style={{cursor:"pointer",width:'36%',display:'inline-block',margin:"23px 20px",border: '4px solid #233b69',padding: '16px',height: '70%',float: 'left',borderRadius: '15px'}}>
              <span class="material-icons" style={{color:'#233b69',fontSize: '30px'}}>
                lock_open
              </span>
              <p style={{fontSize:18,color:'#233b69',fontWeight: 500}}>open</p>
            </div>
            <div onClick={this.pay , () => this.setModal1Visible(true)} style={{cursor:"pointer",width:'36%',display:'inline-block',margin:"23px 20px",border: '4px solid #233b69',padding: '16px',height: '70%',float: 'right',borderRadius: '15px'}}>
              <span class="material-icons" style={{color:'#233b69',fontSize: '30px'}}>
                time_to_leave
              </span>
              <p style={{fontSize:18,color:'#233b69',fontWeight: 500}}>Leave</p>
            </div>    
                  
          </div>
          </div>
          }
          {this.state.reservation.length == 0 &&
          <div>
            <p>You Don't Have Reservation At This Time </p>
            <br></br>
            <NavLink to="/">
              <button 
                  style={{outline:'none',cursor:"pointer",background: "#05A8BB",color: "#fff",border:"none",width: "16%",height: "40px",borderRadius: "25px",fontSize:'18px'}} 
                >   Back To Map
              </button>
            </NavLink>
          </div>
          }
              <Modal 
                width='450px'
                title=''
                visible={this.state.modal1Visible}
                onOk={() => this.setModal1Visible(false)}
                okText='ok'
                cancelText='cancel'
                onCancel={() => this.setModal1Visible(false)}
              >
                {this.state.step == 1 &&
                <div className="container">
                  <form className="white" style={{marginTop:'0px',textAlign:"center"}}>
                  <div className="row">
                      <div className="col s12 m12">
                       <Rater onRate={(val)=>{
                              console.log("RATING  ",val.rating)
                              this.setState({doRate:val.rating})
                        }} total={5} rating={doRate} />
                        <p style={{fontSize: '14px',fontWeight: '500',color:"gray"}}>You had to pay before you go</p>
                        <button 
                          onClick={this.pay}
                          style={{outline:'none',cursor:"pointer",background: "#233b69",color: "#fff",border:"none",width: "46%",height: "30px",borderRadius: "25px"}} 
                        >   Show Reciept
                        </button>
                        

                      </div>
                    </div>
                  </form>
                </div>
                }
                {this.state.step == 2 &&
                  <div className="container">
                  <form className="white" style={{marginTop:'0px'}}>
                    <p style={{fontSize: '30px',fontWeight: '500',color:"#000000",marginBottom:'30px'}}>Reciept</p>
                    <div className="row">
                      <div className="col s12 m12">
                        <p style={{fontSize: '18px',fontWeight: '500',borderBottom: '1px solid #233b69'}}>Hours : <span style={{color:'#233b69',marginLeft: '90px'}}>{this.state.hours}:{this.state.mins}</span></p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col s12 m12">
                        <p style={{fontSize: '18px',fontWeight: '500',borderBottom: '1px solid #233b69'}}>Fine : <span style={{color:'#233b69',marginLeft: '102px'}}>{this.state.fine?grages[0].Fine * this.state.fineHours:0} LE</span></p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col s12 m12">
                        <p style={{fontSize: '18px',fontWeight: '500',borderBottom: '1px solid #233b69'}}>Price : <span style={{color:'#233b69',marginLeft: '92px'}}>{grages[0].HourPrice * this.state.numberOfHours} LE</span></p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col s12 m12">
                        <p style={{fontSize: '18px',fontWeight: '500',borderBottom: '1px solid #233b69'}}>Total: <span style={{color:'#233b69',marginLeft: '92px'}}>{grages[0].HourPrice * this.state.numberOfHours + grages[0].Fine * this.state.fineHours} LE</span></p>
                      </div>
                    </div>
                    <p style={{color:'grey',font:12}}>We informed You That The Hour Price Is 
                    <span style={{color:'#05A8BB',display:'block',textAlign:'center',marginTop:'10px'}}>{grages[0].HourPrice}$</span></p>
                    <div style={{textAlign:'center'}}>
                    <button 
                      style={{outline:'none',cursor:"pointer",background: "#233b69",color: "#fff",border:"none",width: "46%",height: "30px",borderRadius: "25px"}} 
                    >   Pay
                    </button>
                    <PaypalBtn   client={client} currency={'USD'} total={grages[0].HourPrice * this.state.numberOfHours + grages[0].Fine * this.state.fineHours} />
                    </div>
                    
                  </form>
                  </div>
                }
             
               
              </Modal>
              <Modal 
                width='450px'
                title=''
                visible={this.state.modal2Visible}
                onOk={() => this.setModal2Visible(false)}
                okText='ok'
                cancelText='cancel'
                onCancel={() => this.setModal2Visible(false)}
              >
                <div className="container">
                  <form className="white" style={{marginTop:'0px',textAlign:"center"}}>
                    <div className="row">
                      <div className="col s12 m12">
                      <p style={{fontSize: '20px',fontWeight: '500',color:"#000000",marginBottom:'30px'}}>Rate Service</p>
                       
                       <Rater onRate={(val)=>{
                              console.log("RATING  ",val.rating)
                              this.setState({doRate:val.rating})
                        }} total={5} rating={doRate} />
                        <br></br>
                       <br></br>
                        <button 
                          onClick={() => this.rate()}
                          style={{outline:'none',cursor:"pointer",background: "#233b69",color: "#fff",border:"none",width: "46%",height: "30px",borderRadius: "25px"}} 
                        >   rate
                        </button>
                      </div>
                    </div>
                  </form>
                </div>

              </Modal>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  // console.log(state);
  return {
    auth: state.firebase.auth,
  }
}

export default connect(mapStateToProps)(Barrier)

