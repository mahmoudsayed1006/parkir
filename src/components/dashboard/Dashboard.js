import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, NavLink } from 'react-router-dom'
import GoogleMapReact from 'google-map-react';
import {InfoWindow} from 'google-map-react';
import firebase from '../../config/fbConfig'
import Geocode from "react-geocode";
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
var geolocation = require('geolocation')

var currentLocation =[]
class Dashboard extends Component {
 state={
   city:"",
   location:[],
   defaultCenter:[30.5803019,32.3069665],
   showInfo:false,
   currentAdress:'',
   grages:[],
   images:[]
   
 }
 constructor(props){
  super(props)
  geolocation.getCurrentPosition(function (err, position) {
    if (err) throw err
    currentLocation = [position.coords.latitude,position.coords.longitude];
  })

  this.handleGoogleMapApi = this.handleGoogleMapApi.bind(this);
}


  handleChange = (e) => {
   this.setState({
    city: e.target.value,
  })

  }
  
  handleSubmit = (e) => {
    if(this.state.city == "ismailia" ){
      console.log(this.state.grages[0].location.latitude)
      console.log(this.state.grages[0].location.longitude)
      this.setState({
        defaultCenter:[this.state.grages[0].location.latitude,this.state.grages[0].location.longitude],
        location:[this.state.grages[0].location.latitude,this.state.grages[0].location.longitude]
      })
    } else{
      alert("city not exist")
    }

  }
  show = () => {
    this.setState({
     showInfo: true
   })
   }
   getCurrentAdress = () =>{
    Geocode.fromLatLng(30.5803019,32.3069665).then(
      response => {
        const address = response.results[2].formatted_address;
        console.log("ADDRESS   ",response);
        this.setState({currentAdress:address})
      },
      error => {
        console.error("ADDRESS ERROR  ",error);
      }
    );
  }
  getData = () =>{
    
    let images =[];
    firebase.database().ref('ParkirApp/GARAGE').on('value', (snapshot) => {
      let grages = [];
      console.log(snapshot.val())
      snapshot.forEach(data => {
        const dataVal = data.val()
        grages.push({
          Address1: dataVal.Address1,
          location: dataVal.Address2,
          Fine:dataVal.Fine,
          HourPrice:dataVal.HourPrice,
          Name:dataVal.Name,
          rate:dataVal.rate
        })
      })
      this.setState({grages:grages})
      firebase.database().ref(`ParkirApp/GARAGE/FCI_Garage/Images`).on('value', (snapshot) => {
        images =[];
        snapshot.forEach(data => {
          const dataVal = data.val()
          images.push({
            image: dataVal.image,
          })
        })
        this.setState({images:images})
      })
      
    })
      
  }
  getLocation = () =>{
    navigator.geolocation.getCurrentPosition(function(position) {
      currentLocation = [position.coords.latitude,position.coords.longitude];
      console.log("Location :",currentLocation);
    });
  }
  componentDidMount(){
    Geocode.setApiKey("AIzaSyAy_TzJaYpsCRQ-x6rXwOdqEqHr6XaU8pY");
    Geocode.setLanguage("en");
    Geocode.setRegion("es");
    Geocode.enableDebug();
    //this.getCurrentAdress()
    this.getData()
  }
  handleGoogleMapApi = (google) => {
    var flightPath = new google.maps.Polyline({
      path: [ { "lat": this.state.location[0], "lng": this.state.location[1] },{ "lat": currentLocation[0], "lng":currentLocation[1]}],
      geodesic: true,
      strokeColor: '#05A8BB',
      strokeOpacity: 1,
      strokeWeight: 4
    });
  
    flightPath.setMap(google.map);
  }
  render() {
    const MarkerMap = ({ color }) =>

      <span class="material-icons" style={{color:color,fontSize:40,cursor:'pointer'}} onClick={this.show}>
      place
      </span>

    const CurrentLocation = ({ color }) =>
      <span class="material-icons" style={{color:color,fontSize:40}}>
      my_location
      </span>


    const { projects, auth, notifications } = this.props;
    if (!auth.uid) return <Redirect to='/signin' /> 
    console.log(this.state.grages[0])
    console.log(this.state.images)

    return (
      <div className="dashboard">
        <div>
          <div className="col s12 m12">
            
            <div className="row" style={{marginBottom:"0px",marginRight:'0px'}}>
              <div className="input-field container" style={{marginBottom: '0rem',marginTop: '10px'}}>
                <input 
                  style={{    
                    width: '70%',
                    marginBottom: '.5rem',
                    border: '1px solid #aaa',
                    borderRadius: '10px',
                    paddingLeft: '10px',
                    marginRight: '1rem',
                    height:'40px'
                  }} 
                    type="text" id='city' placeholder="Enter City" onChange={this.handleChange}/>
                <button 
                  style={{cursor:"pointer",background: "#05A8BB",color: "#fff",border:"none",width: "16%",height: "40px",borderRadius: "6px"}} 
                  onClick={this.handleSubmit} >
                  Search
                </button>
                <span onClick={this.getLocation} class="material-icons" style={{color:'#05A8BB',fontSize:40,position:'absolute',cursor:'pointer'}}>
                refresh
                </span>
              </div>
            </div>
          
           {/*} <ProjectList projects={projects} />*/}
           <div style={{height:590, width: '100%' }}>
              <GoogleMapReact
                bootstrapURLKeys={{ key: "AIzaSyDJUfhBOeBpTkE2nhSiXJxZ2hoEeIG-IWE" }}
                defaultCenter={{
                  lat: this.state.defaultCenter[0],
                  lng: this.state.defaultCenter[1]
                }}
                defaultZoom={8}
                yesIWantToUseGoogleMapApiInternals
                onGoogleApiLoaded={this.handleGoogleMapApi}
              >
                {this.state.location.length >1 &&
                <MarkerMap
                  lat={this.state.location[0]} 
                  lng= {this.state.location[1]}
                  text="My Marker"
                  color='red'
                   
                />
                }
                <CurrentLocation
                  lat={currentLocation[0]} 
                  lng= {currentLocation[1]}
                  text="My Marker"
                  color='#05A8BB'
                />
               
              </GoogleMapReact>
              </div>
              {this.state.showInfo &&
              <div>
              {this.state.grages.map(val=>(
                <div 
              style={{    
                  width: '270px',
                  minHeight: '170px',
                  position: 'absolute',
                  background: '#fff',
                  top: '45%',
                  zIndex: '100',
                  right: '10%',
                  borderRadius: '18px',
                  border: '1px solid grey',
                  padding: '0px 10px 15px 15px'
              }}>
                <p style={{marginBottom:0,marginTop:0,marginTop: '1rem',fontSize: '19px',fontWeight: 500,}}>
                {val.Name}
                <span class="material-icons" style={{color:'#05A8BB',fontSize: '30px',marginLeft: '10px',position: 'absolute',marginTop: '-5px'}}>
                store_mall_directory
                  </span>
                </p>
                <Rater rating={val.rate} total={5} interactive={false} />

                <p style={{fontSize: '14px',color: 'grey',marginBottom:0,marginTop:10}}>{val.Address1}</p>
                <p style={{marginBottom:0,marginTop:0}}>{val.HourPrice}<span style={{fontSize: '14px',color: 'grey'}}> Per hour</span></p>
                
                <div onClick={()=> this.props.history.push('/Book',{data:this.state.grages,images:this.state.images})}
                  style={{
                    cursor: 'pointer',
                    background: 'rgb(255, 255, 255)',
                    color: 'rgb(5, 168, 187)',
                    border: '2px solid #a7aaaa',
                    width: '42%',
                    height: '38px',
                    borderRadius: '25px',
                    padding:'6px 12px',
                    marginTop: '1rem',
                    display: 'inline-block',
                    marginLeft: '10px'
                  }} 
                  >
                    <span class="material-icons" style={{color:'#05A8BB',fontSize:20,position:'absolute'}}>
                    directions_car
                    </span>
                    <span style={{color: '#000',fontSize: '15px',float: 'right',marginRight: '15px'}}> Book </span>
                </div>
                <div
                  style={{
                    cursor: 'pointer',
                    background: 'rgb(5, 168, 187)',
                    color: '#fff',
                    border: '2px solid #fff',
                    width: '47%',
                    height: '38px',
                    borderRadius: '25px',
                    padding:'6px 12px',
                    marginTop: '1rem',
                    display: 'inline-block',
                    marginLeft: '10px'
                  }} 
                  >
                    <span class="material-icons" style={{color:'#fff',fontSize:20,position:'absolute'}}>
                    alt_route
                    </span>
                    <span style={{color: '#fff',fontSize: '15px',float: 'right',marginRight: '0px'}}> Directions</span>
                </div>
              </div>
              ))}
              </div>
              }
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  // console.log(state);
  return {
    projects: state.firestore.ordered.projects,
    auth: state.firebase.auth,
    notifications: state.firestore.ordered.notifications
  }
}

export default connect(mapStateToProps)(Dashboard)
