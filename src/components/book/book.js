import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect,NavLink } from 'react-router-dom'
import {Tabs,DatePicker,message } from 'antd';

import "antd/dist/antd.css";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import './book.css';
import {  TimePicker} from '@progress/kendo-react-dateinputs'
import '@progress/kendo-react-intl'
import '@progress/kendo-react-tooltip'
import '@progress/kendo-react-common'
import '@progress/kendo-react-popup'
import '@progress/kendo-react-buttons'
import '@progress/kendo-date-math'
import '@progress/kendo-react-dropdowns'
import firebase from '../../config/fbConfig'


const { TabPane } = Tabs;
let fromDate="" ,toDate ="";
class Book extends Component {
  state = { 
    fromTime:"",
    toTime:"",
    fromDate:fromDate,
    toDate:toDate,
    data:this.props.location.state.data,
    images:this.props.location.state.images,
    lan:"",
    disabled:"true",
  }
  constructor(props){
    super(props)
    console.log(this.props.auth.uid)
    
  }
  changeFromTime = (e) => {
    let value ="";
    value = e.target.value.toString();
    console.log(value.substr(16,5))
    this.setState({
      fromTime: value.substr(16,5),
    })
  }
  changeToTime = (e) => {
    let value ="";
    value = e.target.value.toString();
    console.log(value.substr(16,5))
    this.setState({
      toTime: value.substr(16,5),
    })
  }
  changeFromDate (date,dateString) {
   
      fromDate  = dateString
      console.log(fromDate)
  }
  changeToDate(date,dateString) {
      toDate = dateString
      console.log(toDate)
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let l = message.loading('Wait', 2.5)
    console.log(this.state.fromTime)
    console.log(this.state.toTime)
    console.log(fromDate)
    console.log(toDate)
    if(this.state.fromTime != "" && this.state.toTime && fromDate != "" && toDate != ""){
    let flag=0;
    firebase.database().ref(`ParkirApp/iot/FCI_Garage/1`)
    .on('value', (snapshot) => {
      console.log(snapshot.val())
      let data1 = snapshot.val()
      console.log(data1.state)

      if(data1.state == 0){
        this.setState({
          lan: "1"
        })
        console.log(this.state.lan)

      }else{
        firebase.database().ref(`ParkirApp/iot/FCI_Garage/2`)
        .on('value', (snapshot) => {
          console.log(snapshot.val())
          const data2 = snapshot.val()
          if(data2.state == 0){
            this.setState({
              lan: "2"
            })
          }else{
            firebase.database().ref(`ParkirApp/iot/FCI_Garage/3`)
            .on('value', (snapshot) => {
              console.log(snapshot.val())
              const data3 = snapshot.val()
              if(data3.state == 0){
                this.setState({
                  lan: "3"
                })
              }else{
                flag = 1;
              }
            })
          }
        })
      }
    console.log(this.state.lan)
    let intLan =parseInt(this.state.lan);
    console.log(intLan)
    if(flag == 0){
      firebase.database().ref(`ParkirApp/Book/FCI_Garage/${this.state.lan}`).child(this.props.auth.uid)/*.push({*/.set({
        startTime: this.state.fromTime,
        endTime: this.state.toTime,
        startDate: fromDate,
        endDate: toDate,
      }).then(() => {
        let uid = this.props.auth.uid;
        console.log(uid)
        firebase.database().ref(`ParkirApp/users/FCI_Garage`).child(uid).set({
          startTime: this.state.fromTime,
          endTime: this.state.toTime,
          startDate: fromDate,
          endDate: toDate,
          checkPAY:false,
          lan: intLan,
          id:uid
        })
        .then(() => {
          l.then(() => message.success('your garage place is one', 2.5))
          setTimeout(() => {  
            this.props.history.push('/')
            window.location.reload();
           }, 3000);
          
        }).catch(err => {
          //console.log(err.response)
          l.then(() => message.error('err', 2.5))
        });
      }).catch(err => {
        console.log(err.response)
      });
  
    } else{
      l.then(() => message.error('sorry no free lan', 2.5))
    }
  })
}else{
  l.then(() => message.error('please full all inputs', 2.5))
}
    


  }
  render() {
    console.log(this.state.data)
    console.log(this.state.images)
    const { authError, auth } = this.props;
    const { data, images } = this.state;
    if (!auth.uid) return <Redirect to='/signin' /> 
    return ( 
      <div>
      <div  className = "slide" style={{marginTop:'0px',marginBottom:'2rem'}}>
        
      <Carousel
  additionalTransfrom={0}
  arrows
  autoPlaySpeed={3000}
  centerMode={false}
  className=""
  dotListClass=""
  draggable
  focusOnSelect={false}
  infinite
  itemClass=""
  keyBoardControl
  minimumTouchDrag={80}
  renderButtonGroupOutside={false}
  renderDotsOutside={false}
  responsive={{
    desktop: {
      breakpoint: {
        max: 3000,
        min: 1024
      },
      items: 1
    },
    mobile: {
      breakpoint: {
        max: 464,
        min: 0
      },
      items: 1
    },
    tablet: {
      breakpoint: {
        max: 1024,
        min: 464
      },
      items: 1
    }
  }}
  showDots
  sliderClass=""
  slidesToSlide={1}
  swipeable
>
{this.state.images.map(val=>(
  <img
    src={val.image}
    style={{
      display: 'block',
      height: '100%',
      margin: 'auto',
      width: '100%'
    }}
  />
))}
</Carousel>
      </div>
      <div className="container">
        <div className="row">
        <div className="col s12 m12">
         
          <Tabs defaultActiveKey="1">
            <TabPane tab="Garage information" key="1">
              <div className="tabInfo">
                <p style={{textAlign:'center',marginBottom:30,marginTop:10}} className="tabTitle">{data[0].Name}</p>
                <p style={{marginBottom: '2rem'}}>
                  <span class="material-icons" style={{color:'#fff',fontSize: '30px',marginLeft: '10px',position:'absolute'}}>
                  place
                  </span>
                  <span className="spanInfo">{data[0].Address1}</span>
                </p>
                <p style={{marginBottom: '2rem'}}>
                  <span class="material-icons" style={{color:'#fff',fontSize: '30px',marginLeft: '10px',position:'absolute'}}>
                  attach_money
                  </span>
                  <span className="spanInfo">pay {data[0].HourPrice}$ per hour</span>
                </p>
                <p style={{marginBottom: '2rem'}}>
                  <span class="material-icons" style={{color:'#fff',fontSize: '30px',marginLeft: '10px',position:'absolute'}}>
                  credit_card
                  </span>
                  <span className="spanInfo">fine for late {data[0].Fine}$ for hour</span>
                </p>
                <NavLink to="/">
                <button 
                    style={{outline:'none',cursor:"pointer",color: 'rgb(5, 168, 187)',
                    background: 'rgb(255, 255, 255)',border:"none",width: "200px",height: "40px",borderRadius: "25px"}} 
                 >   Back To Map
                </button>
                </NavLink>
              </div>
            </TabPane>
            <TabPane tab="Book Now" key="2">
              <div className="tabInfo" style={{textAlign:'center'}}> 
              <p style={{fontSize:18,marginBottom:30,marginTop:10,fontWeight:400}}>Choose booking time</p>
              <form onSubmit={this.handleSubmit} style={{marginTop:10,padding:0}}>
                <div className="col s12 m6">
                  <div className="input-field">
                    <span style={{margin:'10px',fontSize: '15px',fontWeight: '500'}}>From Time : </span>
                    <TimePicker 
                      onChange={this.changeFromTime}
                      format={{
                        hour: "2-digit",
                        minute: "2-digit"
                      }}
                    />
                  </div>
                </div>
                <div className="col s12 m6">
                  <div className="input-field">
                    <span style={{margin:'10px',fontSize: '15px',fontWeight: '500'}}>To Time : </span>
                    <TimePicker 
                      onChange={this.changeToTime}
                      format={{
                        hour: "2-digit",
                        minute: "2-digit"
                      }}
                    />
                  </div>
                </div>
                <div className="col s12 m6">
                  <div className="input-field">
                  <span style={{margin:'10px',fontSize: '15px',fontWeight: '500'}}>From Date : </span>
                  <DatePicker placeholder="Select From Date"
                  format='DD/MM/YYYY'
                    onChange={this.changeFromDate} />
                  </div>
                </div>
                <div className="col s12 m6">
                  <div className="input-field">
                  <span style={{margin:'10px',fontSize: '15px',fontWeight: '500'}}>To Date : </span>
                  <DatePicker placeholder="Select End Date"
                  format='DD/MM/YYYY'
                    onChange={this.changeToDate} />
                  </div>
                </div>
                <button 
                    //disabled = {this.state.disabled?"true":"false"}
                    style={{outline:'none',cursor:"pointer",color: 'rgb(5, 168, 187)',
                    background: 'rgb(255, 255, 255)',border:"none",width: "200px",height: "40px",borderRadius: "25px"}} 
                    onClick={this.handleSubmit} >
                    Book
                </button>
              </form>
              </div>
            </TabPane>
            
          </Tabs>
        </div>
        </div>
      </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  // console.log(state);
  return {
    auth: state.firebase.auth,
  }
}

const mapDispatchToProps = (dispatch) => {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(Book)
