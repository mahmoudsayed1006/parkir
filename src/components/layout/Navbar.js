import { Link } from 'react-router-dom'
import SignedInLinks from './SignedInLinks'
import SignedOutLinks from './SignedOutLinks'
import { connect } from 'react-redux'
import './style.css'
import React, { Component } from 'react'
import firebase from '../../config/fbConfig'
import { NavLink } from 'react-router-dom'

class Navbar extends Component {
  state={
     reservation : [],
     grage:[]
  }
  constructor(props){
    super(props)
    this.getData()
    this.getGarage()
  }
  getGarage = () =>{
    let arr1 = []
    firebase.database().ref('ParkirApp/GARAGE/FCI_Garage').on('value', (snapshot) => {
      arr1 = [];
      console.log(snapshot.val())
        arr1.push(snapshot.val())
        this.setState({
          grage: arr1,
        })
    })
  }
  getData = () =>{
    let uid = this.props.auth.uid;
      let arr =[];
      firebase.database().ref(`ParkirApp/Book/FCI_Garage/1/${uid}`).on('value', (snapshot) => {
        if (snapshot.val()) {
          console.log(snapshot.val())
          arr =[];
          arr.push(snapshot.val())
          this.setState({
            reservation: arr
          })
        }else{
          firebase.database().ref(`ParkirApp/Book/FCI_Garage/2/${uid}`).on('value', (snapshot) => {
            if (snapshot.val()) {
              console.log(snapshot.val())
              arr =[];
              arr.push(snapshot.val())
              this.setState({
                reservation: arr
              })
            }else{
              firebase.database().ref(`ParkirApp/Book/FCI_Garage/3/${uid}`).on('value', (snapshot) => {
                if (snapshot.val()) {
                  console.log(snapshot.val())
                  arr =[];
                  arr.push(snapshot.val())
                  this.setState({
                    reservation: arr
                  })
                }
              })
            }
          })
        }
        console.log(arr)
        this.setState({
          reservation: arr
        })
      })

  }
  componentDidMount() {
    
    
  }
  render() {
  const { auth, profile } = this.props;
  const {reservation,grage} = this.state
  console.log(reservation,grage);
  const links = auth.uid ? <SignedInLinks data={reservation} data2={grage}/> : <SignedOutLinks />;

  return (
    <nav className="nav-wrapper" style={{background:'#05A8BB'}}>
      <div className="container">
        <Link to='/' className="brand-logo" style={{fontSize:'20px'}}>
          <img width="100px" src="https://res.cloudinary.com/boody-car/image/upload/v1596664315/k3hoj0dalvjov2xvthce.png"></img>
        </Link>
        {links}
      </div>
    </nav>
  )
  }
}

const mapStateToProps = (state) => {
  // console.log(state);
  return{
    auth: state.firebase.auth,
  }
}

export default connect(mapStateToProps)(Navbar)
