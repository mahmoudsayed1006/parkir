import React from 'react'
import { NavLink,Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { signOut } from '../../store/actions/authActions'

const SignedInLinks = (props) => {
  const {data,data2} = props;
  console.log(data)
  console.log(data2)
  return (
    <div>
      <ul className="right">
        <li><a onClick={props.signOut}>Log Out</a></li> 
        <li><Link to={{pathname: "/Barriers",data: data ,data2:data2 }}>Barriers</Link></li>
        {/* props.history.push('/Barriers',{data:data}) */}
      </ul>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}

export default connect(null, mapDispatchToProps)(SignedInLinks)
